package main

import (
	"fmt"
	"os"

	"github.com/alexsergivan/transliterator"
)

func check(err error) {
	if err != nil {
		fmt.Println(err)
	}
}

func main() {
	data, err := os.ReadFile("module2/stl/files/write/example.txt")
	check(err)

	trans := transliterator.NewTransliterator(nil)
	dat := trans.Transliterate(string(data), "en")

	x, err := os.OpenFile("module2/stl/files/write/example.processed.txt", os.O_APPEND|os.O_WRONLY, 0600)
	check(err)
	defer x.Close()
	_, err = x.WriteString(dat)
	check(err)
}
