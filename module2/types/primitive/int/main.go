package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")
	typeInt()

}
func typeInt() {
	fmt.Println("=== START type uint ===")
	var uintNumber uint8 = 1 << 7 //делаем сдвиг на середину в uint8
	var min = int8(uintNumber)    //преобразовываем в int8
	uintNumber--                  //минусуем одно значение
	var max = int8(uintNumber)    //преобразовываем в int8
	fmt.Println(min, max)         //// вывод в stdout (в терминал)
	fmt.Println("int8 min value:", min, "int8 max value:", max, unsafe.Sizeof(min), "bytes")
	var uintNumber16 uint16 = 1 << 15 //делаем сдвиг на середину в uint16
	var min16 = int16(uintNumber16)   //преобразовываем в int16
	uintNumber16--                    //минусуем одно значение
	var max16 = int16(uintNumber16)   //преобразовываем в int16
	fmt.Println(min16, max16)         //// вывод в stdout (в терминал)
	fmt.Println("int16 min value:", min16, "int16 max value:", max16, unsafe.Sizeof(min16), "bytes")
	var uintNumber32 uint32 = 1 << 31 //делаем сдвиг на середину в uint32
	var min32 = int32(uintNumber32)   //преобразовываем в int32
	uintNumber32--                    //минусуем одно значение
	var max32 = int32(uintNumber32)   //преобразовываем в int32
	fmt.Println(min32, max32)         //// вывод в stdout (в терминал)
	fmt.Println("int32 min value:", min32, "int32 max value:", max32, unsafe.Sizeof(min32), "bytes")
	var uintNumber64 uint64 = 1 << 63 //делаем сдвиг на середину в uint64
	var min64 = int64(uintNumber64)   //преобразовываем в int64
	uintNumber64--                    //минусуем одно значение
	var max64 = int64(uintNumber64)   //преобразовываем в int64
	fmt.Println(min64, max64)         //// вывод в stdout (в терминал)
	fmt.Println("int64 min value:", min64, "int32 max value:", max64, unsafe.Sizeof(min64), "bytes")
	fmt.Println("=== END type uint ===")
}
