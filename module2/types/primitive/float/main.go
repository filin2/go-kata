package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeFloat()
}
func typeFloat() {
	fmt.Println("=== START type float ===")
	var floatNumber float32
	// рассчитываем экспоненту согласно схеме
	var uintNumber uint32 = 1 << 29 // осуществляем сдвиг на 29 ячейку
	uintNumber += 1 << 28           // добавляем 1 в 29 позицию
	uintNumber += 1 << 27           // добавляем 1 в 28 позицию
	uintNumber += 1 << 26           // добавляем 1 в 27 позицию
	uintNumber += 1 << 25           // добавляем 1 в 26 позицию
	// добавляем мантиссу
	uintNumber += 1 << 21 //добавляем 1 в 21 позицию
	uintNumber += 1 << 31 // выставляем 32 бит в 1, для отображения значения
	floatNumber = *(*float32)(unsafe.Pointer(&uintNumber))
	fmt.Println(floatNumber)
	a, b := 2.3329, 3.1234
	c := a + b
	fmt.Println("пример ошибки 1:", c)

	a = 9.99999
	b2 := float64(a)
	fmt.Println("пример ошибки 2:", b2)

	a = 999998455
	b3 := float32(a)
	fmt.Println("пример ошибки 3:", "%f\n", b3)

	a4 := 5.2
	b4 := 4.1
	fmt.Println(a4 + b4)
	fmt.Println((a4 + b4) == 9.3)

	c4 := 5.2
	d4 := 2.1
	fmt.Println(c4 + d4)
	fmt.Println((c4 + d4) == 7.3)
	fmt.Println("=== END type float ===")
}
