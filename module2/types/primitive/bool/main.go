package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeBool()
}

func typeBool() {
	var b bool
	fmt.Println("Размер в байтах:", unsafe.Sizeof(b)) //размр в байтах: 1
	var u uint8 = 1
	fmt.Println(b)
	b = *(*bool)(unsafe.Pointer(&u))
	fmt.Println(b)
}
