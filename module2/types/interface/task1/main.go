// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	fmt.Println("Является ли интерфейсное значение nil:", n == nil)
	test(n)
}

func test(r interface{}) {
	switch x := r.(type) { // используем  type-switch
	case *int:
		if r.(*int) == nil {
			fmt.Println("Success!")
		}

	default:
		fmt.Println("default", x)
	}
	if r == nil {
		fmt.Println("Что было Success!")
	}
}
