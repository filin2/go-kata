package main

import "fmt"

type User struct {
	ID   int
	Name string
}

func (u *User) GetName() string {
	return u.Name
}

type Userer interface {
	GetName() string
}

func main() {
	var i Userer = &User{} // пришлось исправить не только 22 строчку и 21, так как ругался линтер
	_ = i
	fmt.Println("Success!")
}
