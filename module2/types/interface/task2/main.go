// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
)

type User struct {
	ID   int
	Name string
}

func (u *User) GetName() string {
	return u.Name
}

type Userer interface {
	GetName() string
}

func main() {
	var u Userer = &User{ // линтер выдавал ошибку для обьединения переменной с присваиванием
		ID:   34,
		Name: "Annet",
	}
	user := u.(*User)   // требуется дописать указатель для реализации
	testUserName(*user) // потворный указатель
}

func testUserName(u User) {
	if u.GetName() == "Annet" {
		fmt.Println("Success!")
	}
}
