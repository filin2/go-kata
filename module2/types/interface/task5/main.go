package main

import "fmt"

type User struct {
	ID   int
	Name string
}

func (u *User) GetName() string { //добавление указателя
	return u.Name
}

type Userer interface {
	GetName() string
}

func main() {
	var i Userer = &User{} //ругался линтер, пришлось сделать изменение и добавление указателя
	_ = i
	fmt.Println("Success!")
}
