package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Eva",
			Age:  13,
		},
		{
			Name: "Victor",
			Age:  28,
		},
		{
			Name: "Dex",
			Age:  34,
		},
		{
			Name: "Billy",
			Age:  21,
		},
		{
			Name: "Foster",
			Age:  29,
		},
	}
	subUsers := users[2:]                         //оригинальный код 	subUsers := users[2:len(users)]
	users2 := append(subUsers[:0:0], subUsers...) // копируем значение среза subUsers в новый массив
	editSecondSlice(users2)

	fmt.Println(users)
}

func editSecondSlice(users []User) {
	for i := range users {
		users[i].Name = "unknown"
	}

}
