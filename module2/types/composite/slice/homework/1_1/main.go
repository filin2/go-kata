package main

import (
	"fmt"
)

func main() {
	s := []int{1, 2, 3}
	s = Append(s, 4)
	fmt.Println(s)
}

func Append(s []int, items ...int) []int {
	for _, item := range items {
		s = Extend(s, item)
	}
	return s
}
func Extend(slice []int, element int) []int {
	n := len(slice)
	if n == cap(slice) {

		newSlice := make([]int, len(slice), 2*len(slice)+1)
		copy(newSlice, slice)
		slice = newSlice
	}
	slice = slice[0 : n+1]
	slice[n] = element
	return slice
}
