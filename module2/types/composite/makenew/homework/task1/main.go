package main

import "fmt"

func main() {
	// var n *int оригинальный код
	var n = new(int) // используем функцию "new" для получения указателя типа
	fmt.Println(*n)
}
