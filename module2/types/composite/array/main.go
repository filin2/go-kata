package main

import "fmt"

func main() {
	tetsArray()
	rangeArray()
}

type User struct {
	Age    int
	Name   string
	Wallet Wallet
}

type Wallet struct {
	RUB uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func tetsArray() {
	// создаем массив
	a := [...]int{34, 55, 89, 144}
	// выводим массив в терминале
	fmt.Println("original value:", a)
	a[0] = 21
	fmt.Println("changed first element:", a)
	b := a
	a[0] = 233
	fmt.Println("original array", a) // [233 55 89 144]
	fmt.Println("modified array", b) // [21 55 89 144]
}

func rangeArray() {
	users := [4]User{
		{
			Age:  13,
			Name: "Кирилл",
			Wallet: Wallet{
				RUB: 13,
				USD: 1,
				BTC: 0,
				ETH: 0,
			},
		},
		{
			Age:  18,
			Name: "Валентина",
			Wallet: Wallet{
				RUB: 144,
				USD: 3,
				BTC: 0,
				ETH: 0,
			},
		},
		{
			Age:  21,
			Name: "Коля",
			Wallet: Wallet{
				RUB: 0,
				USD: 300,
				BTC: 1,
				ETH: 4,
			},
		},
		{
			Age:  26,
			Name: "Анна",
			Wallet: Wallet{
				RUB: 87129,
				USD: 489,
				BTC: 120,
				ETH: 23,
			},
		},
	}
	// Выведим пользоватпелей старше 18
	fmt.Println("Пользователи страше 18 лет:")
	for i := range users {
		if users[i].Age > 18 { //проверка возраста на сколько лет
			fmt.Println(users[i])
		}
	}
	// Выведем у кого есть деньги на кошельке криптовалют
	fmt.Println("Пользоваетли у которых на кошельках криптоволют больше 1")
	for i, user := range users {
		if users[i].Wallet.BTC > 0 || users[i].Wallet.ETH > 0 {
			fmt.Println("users:", user, "index:", i)
		}
	}
	fmt.Println("=== END users non zero crypto balance ===§")
}
