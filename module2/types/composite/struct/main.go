package main

import (
	"fmt"
	"unsafe"
)

type User struct {
	Age      int
	Name     string
	Wallet   Wallet
	Location Location
}
type Location struct {
	Address string
	City    string
	index   string
}

type Wallet struct {
	RUB uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func main() {
	user := User{
		Age:  13,
		Name: "Александр",
	}
	fmt.Println(user)
	wallet := Wallet{
		RUB: 250000,
		USD: 3500,
		BTC: 1,
		ETH: 4,
	}
	fmt.Println(wallet)
	fmt.Println("wallet allocates:", unsafe.Sizeof(wallet), "bytes")
	user.Wallet = wallet
	fmt.Println(user)

	user2 := User{
		Age:  18,
		Name: "Артем",
		Wallet: Wallet{
			RUB: 144000,
			USD: 1330,
			BTC: 55,
			ETH: 34,
		},
		Location: Location{
			Address: "Соколовая ул, 12, к2",
			City:    "Москва",
			index:   "108366",
		},
	}
	fmt.Println(user2)
}
