package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		// сюда впишите ваши остальные 12 структур
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/docker/awesome-compose",
			Stars: 20800,
		},
		{
			Name:  "https://github.com/docker/cli",
			Stars: 3800,
		},
		{
			Name:  "https://github.com/docker/getting-started",
			Stars: 2584,
		},
		{
			Name:  "https://github.com/docker/docker-install",
			Stars: 1430,
		},
		{
			Name:  "https://github.com/docker/docker-py",
			Stars: 6700,
		},
		{
			Name:  "https://github.com/docker/labs",
			Stars: 11080,
		},
		{
			Name:  "https://github.com/GoesToEleven/GolangTraining",
			Stars: 8700,
		},
		{
			Name:  "https://github.com/astaxie/build-web-application-with-golang",
			Stars: 47300,
		},
		{
			Name:  "https://github.com/golangci/golangci-lint",
			Stars: 11100,
		},
		{
			Name:  "https://github.com/Alikhll/golang-developer-roadmap",
			Stars: 16100,
		},
		{
			Name:  "https://github.com/geektutu/7days-golang",
			Stars: 12300,
		},
		{
			Name:  "https://github.com/caddyserver/caddy",
			Stars: 45500,
		},
	}

	tabl := make(map[string]Project)
	for ok := range projects {
		tabl[projects[ok].Name] = projects[ok]
	}
	fmt.Print(tabl)
	for key := range tabl {
		fmt.Println(tabl[key])
	}
}
