package main

import "testing"

func BenchmarkSimplest1(b *testing.B) {
	products := genProducts()
	user := genUsers()
	for i := 0; i < b.N; i++ {
		MapUserProducts(user, products)
	}
}
func BenchmarkSimplest2(b *testing.B) {
	products := genProducts()
	user := genUsers()
	for i := 0; i < b.N; i++ {
		MapUserProducts2(user, products)
	}
}
