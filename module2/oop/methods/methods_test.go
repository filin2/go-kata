package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

type testCase struct {
	x    float64
	y    float64
	want float64
}

func TestMultiply(t *testing.T) {
	testCases := []testCase{
		{2, 2, 4},
		{4, 3, 12},
		{0, 5, 0},
		{50, 10, 500},
		{5, 25, 125},
		{3, -9, -27},
		{-60, 3, -180},
		{0.25, 0.4, 0.1},
	}
	for _, test := range testCases {
		wantResult := test.want

		needResult := multiply(test.x, test.y)
		assert.Equal(t, wantResult, needResult, fmt.Sprintf("expected: %v, got: %v, number1: %v, number2: %v", wantResult, needResult, test.x, test.y))
	}
}
func TestDivide(t *testing.T) {
	testCases := []testCase{
		{2, 2, 1},
		{12, 3, 4},
		{0, 5, 0},
		{50, 10, 5},
		{5, 25, 0.2},
		{3, -9, -0.3333333333333333},
		{-60, 15, -4},
		{0.25, 0.4, 0.625},
	}
	for _, test := range testCases {
		wantResult := test.want
		needResult := Divide(test.x, test.y)
		assert.Equal(t, wantResult, needResult, fmt.Sprintf("expected: %v, got: %v, number1: %v, number2: %v", wantResult, needResult, test.x, test.y))
	}
}

func TestSum(t *testing.T) {
	testCases := []testCase{
		{2, 2, 4},
		{4, 3, 7},
		{0, 5, 5},
		{50, 10, 60},
		{5, 25, 30},
		{3, -9, -6},
		{-60, 15, -45},
		{0.25, 0.4, 0.65},
	}
	for _, test := range testCases {
		wantResult := test.want
		needResult := Sum(test.x, test.y)
		assert.Equal(t, wantResult, needResult, fmt.Sprintf("expected: %v, got: %v, number1: %v, number2: %v", wantResult, needResult, test.x, test.y))
	}
}

func TestAverage(t *testing.T) {
	testCases := []testCase{
		{2, 2, 2},
		{4, 3, 3.5},
		{0, 5, 2.5},
		{50, 10, 30},
		{5, 25, 15},
		{3, -9, -3},
		{-60, 15, -22.5},
		{0.25, 0.4, 0.325},
	}
	for _, test := range testCases {
		wantResult := test.want
		needResult := Average(test.x, test.y)
		assert.Equal(t, wantResult, needResult, fmt.Sprintf("expected: %v, got: %v, number1: %v, number2: %v", wantResult, needResult, test.x, test.y))
	}
}
