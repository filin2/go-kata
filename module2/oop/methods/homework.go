package main

import "fmt"

type calc struct {
	a, b   float64
	result float64
}

func NewCalc() *calc { // конструктор калькулятора
	return &calc{}
}

func (c *calc) SetA(a float64) *calc {
	c.a = a
	return c
}

func (c *calc) SetB(b float64) *calc {
	c.b = b
	return c
}

func (c *calc) Do(operation func(a, b float64) float64) *calc {
	c.result = operation(c.a, c.b)
	return c
}

func (c calc) Result() float64 {
	return c.result
}

func multiply(a, b float64) float64 { // реализуйте по примеру divide, sum, average
	return a * b
}

func Divide(a, b float64) float64 { // реализуйте по примеру divide, sum, average
	return a / b
}

func Sum(a, b float64) float64 { // реализуйте по примеру divide, sum, average
	return a + b
}

func Average(a, b float64) float64 { // реализуйте по примеру divide, sum, average
	return (a + b) / 2
}

func main() {
	calc := NewCalc()
	res := calc.SetA(10).SetB(34).Do(multiply).Result()
	fmt.Println("Multiplication:", res)
	res22 := calc.SetA(9).SetB(2).Do(Divide).Result()
	fmt.Println("Divide:", res22)
	res3 := calc.SetA(10).SetB(34).Do(Sum).Result()
	fmt.Println("Sum:", res3)
	res4 := calc.SetA(10).SetB(34).Do(Average).Result()
	fmt.Println("Average:", res4)
	res2 := calc.Result()
	fmt.Println(res2)
	if res4 != res2 {
		panic("object statement is not persist")
	}

}
