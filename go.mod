module gitlab.com/filin2/go-kata

go 1.19

require (
	github.com/brianvoe/gofakeit/v6 v6.20.1
	github.com/stretchr/testify v1.8.2
	gitlab.com/filin2/greet v0.0.0-20230208065352-849d6a3eec50
)

require (
	github.com/alexsergivan/transliterator v1.0.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
